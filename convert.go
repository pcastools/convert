// Convert defines functions for converting between basic data types.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package convert

import (
	"database/sql/driver"
	"fmt"
	"math"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// MaxInt defines the maximum size of an int.
const MaxInt = int64(int(^uint(0) >> 1))

// MinInt defines the minimum size of an int.
const MinInt = -MaxInt - 1

// MaxUint defines the maximum size of a uint.
const MaxUint = uint64(^uint(0))

// Error messages.
const (
	msgNonValueType = "non-Value type %T returned from Value."
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// float64ToInt64 attempts to convert the given float64 to an int64.
func float64ToInt64(f float64) (int64, error) {
	if math.IsNaN(f) {
		return 0, os.ErrInvalid
	} else if math.IsInf(f, 0) {
		return 0, strconv.ErrRange
	} else if math.Floor(f) != f {
		return 0, os.ErrInvalid
	} else if f >= math.MinInt64 && f <= math.MaxInt64 {
		return int64(f), nil
	}
	return 0, strconv.ErrRange
}

// float64ToUint64 attempts to convert the given float64 to a uint64.
func float64ToUint64(f float64) (uint64, error) {
	if math.IsNaN(f) {
		return 0, os.ErrInvalid
	} else if math.IsInf(f, 0) {
		return 0, strconv.ErrRange
	} else if math.Floor(f) != f {
		return 0, os.ErrInvalid
	} else if f >= 0 && f <= math.MaxUint64 {
		return uint64(f), nil
	}
	return 0, strconv.ErrRange
}

// toValue attempts to convert the given object to a driver.Value.
func toValue(obj interface{}) (ok bool, v driver.Value, err error) {
	var objv driver.Valuer
	if objv, ok = obj.(driver.Valuer); ok {
		if v, err = objv.Value(); err != nil && !driver.IsValue(v) {
			err = fmt.Errorf(msgNonValueType, v)
		}
	}
	return
}

// toString attempts to convert the given object to a string.
func toString(obj interface{}) (ok bool, s string) {
	var objs fmt.Stringer
	if objs, ok = obj.(fmt.Stringer); ok {
		s = objs.String()
	}
	return
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ToInt8 attempts to return the object as an int8.
func ToInt8(obj interface{}) (val int8, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		val = data
	case int16:
		if data >= math.MinInt8 && data <= math.MaxInt8 {
			val = int8(data)
		} else {
			err = strconv.ErrRange
		}
	case int32:
		if data >= math.MinInt8 && data <= math.MaxInt8 {
			val = int8(data)
		} else {
			err = strconv.ErrRange
		}
	case int64:
		if data >= math.MinInt8 && data <= math.MaxInt8 {
			val = int8(data)
		} else {
			err = strconv.ErrRange
		}
	case int:
		if data >= math.MinInt8 && data <= math.MaxInt8 {
			val = int8(data)
		} else {
			err = strconv.ErrRange
		}
	case uint8:
		if data <= math.MaxInt8 {
			val = int8(data)
		} else {
			err = strconv.ErrRange
		}
	case uint16:
		if data <= math.MaxInt8 {
			val = int8(data)
		} else {
			err = strconv.ErrRange
		}
	case uint32:
		if data <= math.MaxInt8 {
			val = int8(data)
		} else {
			err = strconv.ErrRange
		}
	case uint64:
		if data <= math.MaxInt8 {
			val = int8(data)
		} else {
			err = strconv.ErrRange
		}
	case uint:
		if data <= math.MaxInt8 {
			val = int8(data)
		} else {
			err = strconv.ErrRange
		}
	case float32:
		var v int64
		if v, err = float64ToInt64(float64(data)); err == nil {
			if v >= math.MinInt8 && v <= math.MaxInt8 {
				val = int8(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case float64:
		var v int64
		if v, err = float64ToInt64(data); err == nil {
			if v >= math.MinInt8 && v <= math.MaxInt8 {
				val = int8(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case bool:
		if data {
			val = 1
		}
	case string:
		var v int64
		v, err = strconv.ParseInt(strings.TrimSpace(data), 10, 8)
		val = int8(v)
	case []byte:
		var v int64
		v, err = strconv.ParseInt(strings.TrimSpace(string(data)), 10, 8)
		val = int8(v)
	case time.Time:
		v := data.Unix()
		if v >= math.MinInt8 && v <= math.MaxInt8 {
			val = int8(v)
		} else {
			err = strconv.ErrRange
		}
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToInt8(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToInt8(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToInt16 attempts to return the object as an int16.
func ToInt16(obj interface{}) (val int16, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		val = int16(data)
	case int16:
		val = data
	case int32:
		if data >= math.MinInt16 && data <= math.MaxInt16 {
			val = int16(data)
		} else {
			err = strconv.ErrRange
		}
	case int64:
		if data >= math.MinInt16 && data <= math.MaxInt16 {
			val = int16(data)
		} else {
			err = strconv.ErrRange
		}
	case int:
		if data >= math.MinInt16 && data <= math.MaxInt16 {
			val = int16(data)
		} else {
			err = strconv.ErrRange
		}
	case uint8:
		val = int16(data)
	case uint16:
		if data <= math.MaxInt16 {
			val = int16(data)
		} else {
			err = strconv.ErrRange
		}
	case uint32:
		if data <= math.MaxInt16 {
			val = int16(data)
		} else {
			err = strconv.ErrRange
		}
	case uint64:
		if data <= math.MaxInt16 {
			val = int16(data)
		} else {
			err = strconv.ErrRange
		}
	case uint:
		if data <= math.MaxInt16 {
			val = int16(data)
		} else {
			err = strconv.ErrRange
		}
	case float32:
		var v int64
		if v, err = float64ToInt64(float64(data)); err == nil {
			if v >= math.MinInt16 && v <= math.MaxInt16 {
				val = int16(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case float64:
		var v int64
		if v, err = float64ToInt64(data); err == nil {
			if v >= math.MinInt16 && v <= math.MaxInt16 {
				val = int16(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case bool:
		if data {
			val = 1
		}
	case string:
		var v int64
		v, err = strconv.ParseInt(strings.TrimSpace(data), 10, 16)
		val = int16(v)
	case []byte:
		var v int64
		v, err = strconv.ParseInt(strings.TrimSpace(string(data)), 10, 16)
		val = int16(v)
	case time.Time:
		v := data.Unix()
		if v >= math.MinInt16 && v <= math.MaxInt16 {
			val = int16(v)
		} else {
			err = strconv.ErrRange
		}
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToInt16(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToInt16(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToInt32 attempts to return the object as an int32.
func ToInt32(obj interface{}) (val int32, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		val = int32(data)
	case int16:
		val = int32(data)
	case int32:
		val = data
	case int64:
		if data >= math.MinInt32 && data <= math.MaxInt32 {
			val = int32(data)
		} else {
			err = strconv.ErrRange
		}
	case int:
		if data >= math.MinInt32 && data <= math.MaxInt32 {
			val = int32(data)
		} else {
			err = strconv.ErrRange
		}
	case uint8:
		val = int32(data)
	case uint16:
		val = int32(data)
	case uint32:
		if data <= math.MaxInt32 {
			val = int32(data)
		} else {
			err = strconv.ErrRange
		}
	case uint64:
		if data <= math.MaxInt32 {
			val = int32(data)
		} else {
			err = strconv.ErrRange
		}
	case uint:
		if data <= math.MaxInt32 {
			val = int32(data)
		} else {
			err = strconv.ErrRange
		}
	case float32:
		var v int64
		if v, err = float64ToInt64(float64(data)); err == nil {
			if v >= math.MinInt32 && v <= math.MaxInt32 {
				val = int32(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case float64:
		var v int64
		if v, err = float64ToInt64(data); err == nil {
			if v >= math.MinInt32 && v <= math.MaxInt32 {
				val = int32(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case bool:
		if data {
			val = 1
		}
	case string:
		var v int64
		v, err = strconv.ParseInt(strings.TrimSpace(data), 10, 32)
		val = int32(v)
	case []byte:
		var v int64
		v, err = strconv.ParseInt(strings.TrimSpace(string(data)), 10, 32)
		val = int32(v)
	case time.Time:
		v := data.Unix()
		if v >= math.MinInt32 && v <= math.MaxInt32 {
			val = int32(v)
		} else {
			err = strconv.ErrRange
		}
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToInt32(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToInt32(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToInt64 attempts to return the object as an int64.
func ToInt64(obj interface{}) (val int64, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		val = int64(data)
	case int16:
		val = int64(data)
	case int32:
		val = int64(data)
	case int64:
		val = data
	case int:
		val = int64(data)
	case uint8:
		val = int64(data)
	case uint16:
		val = int64(data)
	case uint32:
		val = int64(data)
	case uint64:
		if data <= math.MaxInt64 {
			val = int64(data)
		} else {
			err = strconv.ErrRange
		}
	case uint:
		if uint64(data) <= math.MaxInt64 {
			val = int64(data)
		} else {
			err = strconv.ErrRange
		}
	case float32:
		val, err = float64ToInt64(float64(data))
	case float64:
		val, err = float64ToInt64(data)
	case bool:
		if data {
			val = 1
		}
	case string:
		val, err = strconv.ParseInt(strings.TrimSpace(data), 10, 64)
	case []byte:
		val, err = strconv.ParseInt(strings.TrimSpace(string(data)), 10, 64)
	case time.Time:
		val = data.Unix()
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToInt64(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToInt64(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToInt attempts to return the object as an int.
func ToInt(obj interface{}) (val int, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		val = int(data)
	case int16:
		val = int(data)
	case int32:
		val = int(data)
	case int64:
		if data >= MinInt && data <= MaxInt {
			val = int(data)
		} else {
			err = strconv.ErrRange
		}
	case int:
		val = data
	case uint8:
		val = int(data)
	case uint16:
		val = int(data)
	case uint32:
		if int64(data) <= MaxInt {
			val = int(data)
		} else {
			err = strconv.ErrRange
		}
	case uint64:
		if data <= uint64(MaxInt) {
			val = int(data)
		} else {
			err = strconv.ErrRange
		}
	case float32:
		var v int64
		if v, err = float64ToInt64(float64(data)); err == nil {
			if v >= MinInt && v <= MaxInt {
				val = int(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case float64:
		var v int64
		if v, err = float64ToInt64(data); err == nil {
			if v >= MinInt && v <= MaxInt {
				val = int(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case bool:
		if data {
			val = 1
		}
	case string:
		val, err = strconv.Atoi(data)
	case []byte:
		val, err = strconv.Atoi(string(data))
	case time.Time:
		v := data.Unix()
		if v >= MinInt && v <= MaxInt {
			val = int(v)
		} else {
			err = strconv.ErrRange
		}
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToInt(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToInt(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToUint8 attempts to return the object as a uint8.
func ToUint8(obj interface{}) (val uint8, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		if data >= 0 && int64(data) <= math.MaxUint8 {
			val = uint8(data)
		} else {
			err = strconv.ErrRange
		}
	case int16:
		if data >= 0 && int64(data) <= math.MaxUint8 {
			val = uint8(data)
		} else {
			err = strconv.ErrRange
		}
	case int32:
		if data >= 0 && int64(data) <= math.MaxUint8 {
			val = uint8(data)
		} else {
			err = strconv.ErrRange
		}
	case int64:
		if data >= 0 && int64(data) <= math.MaxUint8 {
			val = uint8(data)
		} else {
			err = strconv.ErrRange
		}
	case int:
		if data >= 0 && int64(data) <= math.MaxUint8 {
			val = uint8(data)
		} else {
			err = strconv.ErrRange
		}
	case uint8:
		val = data
	case uint16:
		if data <= math.MaxUint8 {
			val = uint8(data)
		} else {
			err = strconv.ErrRange
		}
	case uint32:
		if data <= math.MaxUint8 {
			val = uint8(data)
		} else {
			err = strconv.ErrRange
		}
	case uint64:
		if data <= math.MaxUint8 {
			val = uint8(data)
		} else {
			err = strconv.ErrRange
		}
	case uint:
		if data <= math.MaxUint8 {
			val = uint8(data)
		} else {
			err = strconv.ErrRange
		}
	case float32:
		var v uint64
		if v, err = float64ToUint64(float64(data)); err == nil {
			if v <= math.MaxInt8 {
				val = uint8(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case float64:
		var v uint64
		if v, err = float64ToUint64(data); err == nil {
			if v <= math.MaxInt8 {
				val = uint8(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case bool:
		if data {
			val = 1
		}
	case string:
		var v uint64
		v, err = strconv.ParseUint(strings.TrimSpace(data), 10, 8)
		val = uint8(v)
	case []byte:
		var v uint64
		v, err = strconv.ParseUint(strings.TrimSpace(string(data)), 10, 8)
		val = uint8(v)
	case time.Time:
		v := data.Unix()
		if v >= 0 && v <= math.MaxUint8 {
			val = uint8(v)
		} else {
			err = strconv.ErrRange
		}
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToUint8(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToUint8(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToUint16 attempts to return the object as a uint16.
func ToUint16(obj interface{}) (val uint16, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		if data >= 0 && int64(data) <= math.MaxUint16 {
			val = uint16(data)
		} else {
			err = strconv.ErrRange
		}
	case int16:
		if data >= 0 && int64(data) <= math.MaxUint16 {
			val = uint16(data)
		} else {
			err = strconv.ErrRange
		}
	case int32:
		if data >= 0 && int64(data) <= math.MaxUint16 {
			val = uint16(data)
		} else {
			err = strconv.ErrRange
		}
	case int64:
		if data >= 0 && int64(data) <= math.MaxUint16 {
			val = uint16(data)
		} else {
			err = strconv.ErrRange
		}
	case int:
		if data >= 0 && int64(data) <= math.MaxUint16 {
			val = uint16(data)
		} else {
			err = strconv.ErrRange
		}
	case uint8:
		val = uint16(data)
	case uint16:
		val = data
	case uint32:
		if data <= math.MaxUint16 {
			val = uint16(data)
		} else {
			err = strconv.ErrRange
		}
	case uint64:
		if data <= math.MaxUint16 {
			val = uint16(data)
		} else {
			err = strconv.ErrRange
		}
	case uint:
		if data <= math.MaxUint16 {
			val = uint16(data)
		} else {
			err = strconv.ErrRange
		}
	case float32:
		var v uint64
		if v, err = float64ToUint64(float64(data)); err == nil {
			if v <= math.MaxInt16 {
				val = uint16(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case float64:
		var v uint64
		if v, err = float64ToUint64(float64(data)); err == nil {
			if v <= math.MaxInt16 {
				val = uint16(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case bool:
		if data {
			val = 1
		}
	case string:
		var v uint64
		v, err = strconv.ParseUint(strings.TrimSpace(data), 10, 16)
		val = uint16(v)
	case []byte:
		var v uint64
		v, err = strconv.ParseUint(strings.TrimSpace(string(data)), 10, 16)
		val = uint16(v)
	case time.Time:
		v := data.Unix()
		if v >= 0 && v <= math.MaxUint16 {
			val = uint16(v)
		} else {
			err = strconv.ErrRange
		}
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToUint16(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToUint16(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToUint32 attempts to return the object as a uint32.
func ToUint32(obj interface{}) (val uint32, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		if data >= 0 && int64(data) <= math.MaxUint32 {
			val = uint32(data)
		} else {
			err = strconv.ErrRange
		}
	case int16:
		if data >= 0 && int64(data) <= math.MaxUint32 {
			val = uint32(data)
		} else {
			err = strconv.ErrRange
		}
	case int32:
		if data >= 0 && int64(data) <= math.MaxUint32 {
			val = uint32(data)
		} else {
			err = strconv.ErrRange
		}
	case int64:
		if data >= 0 && int64(data) <= math.MaxUint32 {
			val = uint32(data)
		} else {
			err = strconv.ErrRange
		}
	case int:
		if data >= 0 && int64(data) <= math.MaxUint32 {
			val = uint32(data)
		} else {
			err = strconv.ErrRange
		}
	case uint8:
		val = uint32(data)
	case uint16:
		val = uint32(data)
	case uint32:
		val = data
	case uint64:
		if data <= math.MaxUint32 {
			val = uint32(data)
		} else {
			err = strconv.ErrRange
		}
	case uint:
		if data <= math.MaxUint32 {
			val = uint32(data)
		} else {
			err = strconv.ErrRange
		}
	case float32:
		var v uint64
		if v, err = float64ToUint64(float64(data)); err == nil {
			if v <= math.MaxInt32 {
				val = uint32(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case float64:
		var v uint64
		if v, err = float64ToUint64(data); err == nil {
			if v <= math.MaxInt32 {
				val = uint32(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case bool:
		if data {
			val = 1
		}
	case string:
		var v uint64
		v, err = strconv.ParseUint(strings.TrimSpace(data), 10, 32)
		val = uint32(v)
	case []byte:
		var v uint64
		v, err = strconv.ParseUint(strings.TrimSpace(string(data)), 10, 32)
		val = uint32(v)
	case time.Time:
		v := data.Unix()
		if v >= 0 && v <= math.MaxUint32 {
			val = uint32(v)
		} else {
			err = strconv.ErrRange
		}
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToUint32(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToUint32(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToUint64 attempts to return the object as a uint64.
func ToUint64(obj interface{}) (val uint64, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		if data >= 0 {
			val = uint64(data)
		} else {
			err = strconv.ErrRange
		}
	case int16:
		if data >= 0 {
			val = uint64(data)
		} else {
			err = strconv.ErrRange
		}
	case int32:
		if data >= 0 {
			val = uint64(data)
		} else {
			err = strconv.ErrRange
		}
	case int64:
		if data >= 0 {
			val = uint64(data)
		} else {
			err = strconv.ErrRange
		}
	case int:
		if data >= 0 {
			val = uint64(data)
		} else {
			err = strconv.ErrRange
		}
	case uint8:
		val = uint64(data)
	case uint16:
		val = uint64(data)
	case uint32:
		val = uint64(data)
	case uint64:
		val = data
	case uint:
		val = uint64(data)
	case float32:
		val, err = float64ToUint64(float64(data))
	case float64:
		val, err = float64ToUint64(data)
	case bool:
		if data {
			val = 1
		}
	case string:
		val, err = strconv.ParseUint(strings.TrimSpace(data), 10, 64)
	case []byte:
		val, err = strconv.ParseUint(strings.TrimSpace(string(data)), 10, 64)
	case time.Time:
		v := data.Unix()
		if v >= 0 {
			val = uint64(v)
		} else {
			err = strconv.ErrRange
		}
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToUint64(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToUint64(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToUint attempts to return the object as a uint.
func ToUint(obj interface{}) (val uint, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		if data >= 0 && uint64(data) <= MaxUint {
			val = uint(data)
		} else {
			err = strconv.ErrRange
		}
	case int16:
		if data >= 0 && uint64(data) <= MaxUint {
			val = uint(data)
		} else {
			err = strconv.ErrRange
		}
	case int32:
		if data >= 0 && uint64(data) <= MaxUint {
			val = uint(data)
		} else {
			err = strconv.ErrRange
		}
	case int64:
		if data >= 0 && uint64(data) <= MaxUint {
			val = uint(data)
		} else {
			err = strconv.ErrRange
		}
	case int:
		if data >= 0 && uint64(data) <= MaxUint {
			val = uint(data)
		} else {
			err = strconv.ErrRange
		}
	case uint8:
		val = uint(data)
	case uint16:
		val = uint(data)
	case uint32:
		val = uint(data)
	case uint64:
		if data <= MaxUint {
			val = uint(data)
		} else {
			err = strconv.ErrRange
		}
	case uint:
		val = data
	case float32:
		var v uint64
		if v, err = float64ToUint64(float64(data)); err == nil {
			if v <= MaxUint {
				val = uint(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case float64:
		var v uint64
		if v, err = float64ToUint64(data); err == nil {
			if v <= MaxUint {
				val = uint(v)
			} else {
				err = strconv.ErrRange
			}
		}
	case bool:
		if data {
			val = 1
		}
	case string:
		var v uint64
		v, err = strconv.ParseUint(strings.TrimSpace(data), 10, 0)
		val = uint(v)
	case []byte:
		var v uint64
		v, err = strconv.ParseUint(strings.TrimSpace(string(data)), 10, 0)
		val = uint(v)
	case time.Time:
		v := data.Unix()
		if v >= 0 && uint64(v) <= MaxUint {
			val = uint(v)
		} else {
			err = strconv.ErrRange
		}
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToUint(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToUint(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToBool attempts to return the object as a bool.
func ToBool(obj interface{}) (val bool, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		val = data != 0
	case int16:
		val = data != 0
	case int32:
		val = data != 0
	case int64:
		val = data != 0
	case int:
		val = data != 0
	case uint8:
		val = data != 0
	case uint16:
		val = data != 0
	case uint32:
		val = data != 0
	case uint64:
		val = data != 0
	case uint:
		val = data != 0
	case float32:
		val = data != 0
	case float64:
		val = data != 0
	case bool:
		val = data
	case string:
		val, err = strconv.ParseBool(strings.TrimSpace(data))
	case []byte:
		val, err = strconv.ParseBool(strings.TrimSpace(string(data)))
	case time.Time:
		val = !data.IsZero()
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToBool(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToBool(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToFloat64 attempts to return the object as a float64.
func ToFloat64(obj interface{}) (val float64, err error) {
	switch data := obj.(type) {
	case nil:
	case int8, int16, int32, int64, int:
		if v := reflect.ValueOf(data).Int(); v >= 2<<53 || v <= -(2<<53) {
			err = strconv.ErrRange
		} else {
			val = float64(v)
		}
	case uint8, uint16, uint32, uint64, uint:
		if v := reflect.ValueOf(data).Uint(); v >= 2<<53 {
			err = strconv.ErrRange
		} else {
			val = float64(v)
		}
	case float32:
		val = float64(data)
	case float64:
		val = data
	case bool:
		if data {
			val = 1
		}
	case string:
		val, err = strconv.ParseFloat(strings.TrimSpace(data), 64)
	case []byte:
		val, err = strconv.ParseFloat(strings.TrimSpace(string(data)), 64)
	case time.Time:
		v := data.Unix()
		if v >= 2<<53 || v <= -(2<<53) {
			err = strconv.ErrRange
		} else {
			val = float64(v)
		}
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				val, err = ToFloat64(v)
			}
		} else if ok, v = toString(obj); ok {
			val, err = ToFloat64(v)
		} else {
			err = os.ErrInvalid
		}
	}
	return
}

// ToString returns the object as a string.
func ToString(obj interface{}) (str string, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		str = strconv.Itoa(int(data))
	case int16:
		str = strconv.Itoa(int(data))
	case int32:
		str = strconv.Itoa(int(data))
	case int64:
		str = strconv.FormatInt(data, 10)
	case int:
		str = strconv.Itoa(int(data))
	case uint8:
		str = strconv.FormatUint(uint64(data), 10)
	case uint16:
		str = strconv.FormatUint(uint64(data), 10)
	case uint32:
		str = strconv.FormatUint(uint64(data), 10)
	case uint64:
		str = strconv.FormatUint(data, 10)
	case uint:
		str = strconv.FormatUint(uint64(data), 10)
	case float32:
		str = strconv.FormatFloat(float64(data), 'f', -1, 32)
	case float64:
		str = strconv.FormatFloat(data, 'f', -1, 64)
	case bool:
		if data {
			str = "1"
		} else {
			str = "0"
		}
	case string:
		str = data
	case []byte:
		str = string(data)
	default:
		var ok bool
		var v interface{}
		if ok, v, err = toValue(obj); ok {
			if err == nil {
				str, err = ToString(v)
			}
		} else if ok, str = toString(obj); !ok {
			str = fmt.Sprint(data)
		}
	}
	return
}

// ToValue attempts to return the object as a driver.Value.
func ToValue(obj interface{}) (val driver.Value, err error) {
	switch data := obj.(type) {
	case nil:
	case int8:
		val = int64(data)
	case int16:
		val = int64(data)
	case int32:
		val = int64(data)
	case int64:
		val = data
	case int:
		val = int64(data)
	case uint8:
		val = int64(data)
	case uint16:
		val = int64(data)
	case uint32:
		val = int64(data)
	case uint64:
		if data <= math.MaxInt64 {
			val = int64(data)
		} else {
			err = strconv.ErrRange
		}
	case uint:
		if uint64(data) <= math.MaxInt64 {
			val = int64(data)
		} else {
			err = strconv.ErrRange
		}
	case float32:
		val = float64(data)
	case float64:
		val = data
	case bool:
		val = data
	case string:
		val = data
	case []byte:
		val = data
	case time.Time:
		val = data
	default:
		var ok bool
		if ok, val, err = toValue(obj); !ok {
			if ok, val = toString(obj); !ok {
				val = fmt.Sprint(data)
			}
		}
	}
	return
}
